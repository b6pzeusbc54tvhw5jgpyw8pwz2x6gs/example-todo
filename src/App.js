import React from 'react';
import Todo from './Todo';
import logo from './logo.svg';
import Footer from './Footer';

const App = () =>{
  return (
    <div className={ st.App }>
      <div className={ st.AppHeader }>
        <img src={logo} className={ st.AppLogo } alt="logo" />
        <h2>Todo by bh23.yoo</h2>
      </div>
      <div className={ st.appIndexJs }>
        <Todo />
      </div>
      <Footer />
    </div>
  )
};

export default App;

const st = cssInJS({
  'App': {
    textAlign: 'center',
    overflow: 'hidden',
    minWidth: 640,
    boxSizing: 'border-box',
  },

  'AppLogo': {
    animation: 'App-logo-spin infinite 20s linear',
    height: 80,
  },

  'AppHeader': {
    backgroundColor: '#222',
    height: '150px',
    padding: '20px',
    color: 'white',
  },

  'AppIntro': {
    fontSize: 'large',
  },

});
