import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createLogger from 'redux-logger';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import rootReducer from './rootReducer';
import actionHelper from './actionHelper';
import action from './action';
import * as storage from 'redux-storage'
import createEngine from 'redux-storage-engine-localstorage';

const reducer = storage.reducer( rootReducer );
const engine = createEngine('my-save-key');
const localStorageMiddleware = storage.createMiddleware(engine);

const loggerMiddleware = createLogger({ duration: true, collapsed: true });

const createStoreWithMiddleware = applyMiddleware(
  loggerMiddleware,
  localStorageMiddleware,
)( createStore );

const store = createStoreWithMiddleware( reducer );

const load = storage.createLoader(engine);
load( store );

actionHelper.init( store );

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

if( __DEVELOPMENT__ ) {
  require('./animation.css');
  require('../build/style.css');
}

window.__action = action;
window.__store = store;

// eslint-disable-next-line
const st = cssInJS({
  '$body': {
    margin: 0,
    padding: 0,
    fontFamily: 'sans-serif',
    backgroundColor: '#f5f5f5',
  },

  '$input': {
    margin: 0,
    padding: 0,
    boxSizing: 'border-box',
  },

  '$button': {
    margin: 0,
    padding: 0,
  },
});
