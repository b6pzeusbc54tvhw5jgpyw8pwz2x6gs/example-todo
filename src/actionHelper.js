import logger from 'loglevel';

let store;
export function dispatch( ...argList ) {
  if (!store) {
    logger.error('no store');
  }
  store.dispatch( ...argList );
}

export function getState() {
  return store.getState();
}

export function subscribe(...argList) {
  if (!store) {
    logger.error('no store');
  }
  return store.subscribe( ...argList );
}

export const errorHandler = (err)=>{
  logger.error(err);
  logger.error(err.stack);
};

export function env( key ) {
  const val = getState()._env[ key ];
  if ( val === undefined ) {
    return logger.error('no env by key: ' + key);
  }
  return val;
}

export function init( s ) {
  store = s;
}

export default exports;
