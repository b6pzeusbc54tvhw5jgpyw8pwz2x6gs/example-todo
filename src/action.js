import { dispatch } from '_src/actionHelper';
import _ from 'underscore';
import logger from 'loglevel';
import chrono from 'chrono-node';
import parseDuration from 'parse-duration';
import humanDate from 'human-date';

let todoKey = localStorage.todoKey || 1;
todoKey = Number( todoKey );
localStorage.todoKey = todoKey + 1;

export const addTodo = ( text )=>{
  text = text.trim();
  if( ! text ) {
    alert('내용이 없어요');
    return;
  }

  const now = Date.now();
  let expireTime = chrono.parseDate( text );
  if( expireTime ) {
    expireTime = expireTime.getTime();
  } else {
    const duration = parseDuration( text );

    if( duration ) {

      let object = humanDate.relativeTime( duration/1000, { returnObject: true });

      delete object.past;

      console.log( object );
      object = {
        years: object.years,
        days: object.days,
        hours: object.hours,
        minutes: object.minutes,
        seconds: object.seconds,
      };

      const modified = prompt('Correct? from now,', JSON.stringify( object ));

      try {
        object = JSON.parse( modified );

        expireTime = now+ (
          object.years * 1000 * 60 * 60 * 24 * 365 +
          object.days * 1000 * 60 * 60 * 24 +
          object.hours * 1000 * 60 * 60 +
          object.minutes * 1000 * 60 +
          object.seconds * 1000
        );

      } catch( err) {

        logger.warn('수정된 문구 parse error');
        object = null;
      }
    }
  }

  const todo = {
    id: _.uniqueId( todoKey ),
    cTime: Date.now(),
    eTime: expireTime,
    text,
  };
  dispatch({ type: 'ADD_TODO', todo });
};

export const checkTodo = ( id )=>{
  dispatch({ type: 'CHECK_TODO', id });
};

export const deleteTodo = ( id )=>{
  dispatch({ type: 'DELETE_TODO', id });
};

export const changeViewMode = ( viewMode )=>{
  dispatch({ type: 'CHANGE_VIEW_MODE', viewMode });
};
export const clearCompleted = ()=>{
  dispatch({ type: 'CLEAR_COMPLETED' });
};

export default exports;

