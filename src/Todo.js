import React from 'react';
import cx from 'classnames';
import _ from 'underscore';
import { connect } from 'react-redux';
import { deleteTodo, checkTodo, addTodo } from '_src/action';
import humanDate from 'human-date';
import colormap from 'colormap';

const colorList = colormap({
  colormap: 'hsv',
  nshades: 300,
  format: 'rgb',
  alpha: 0.7,
}).slice( 0, 100 );

class RTime extends React.Component {

  componentWillUnmount = ()=>{
    clearInterval( this.timer );
  }

  startTimer = () =>{

    this.timer = setInterval( ()=>{
      this.forceUpdate();
    }, 2000 );
  }

  render() {

    const { eTime, setTodoItemColor } = this.props;

    if( ! eTime ) {
      return <div style={{display:'none'}}></div>;
    }

    if( !this.timer ) {
      this.startTimer();
    }

    const rTime = (eTime - Date.now()) / 1000;    // sec
    const text = humanDate.relativeTime( rTime, { allUnits: true });

    
    //const maxColor = 0;
    const minColor = 3600 * 24 * 15; // 2weaks
    const colorIdx = Math.floor( (rTime / minColor) * 100 );
    const color = colorList[ colorIdx ];
    color && setTodoItemColor( color );

    return (
      <div className={st.rTime}>{ text }</div>
    );
  }
};

const CheckBox = props=>{

  const cn = cx( st.checkBox, {
    [st.checkBox_checked]: props.checked,
  });
  return (
    <input type="checkbox" className={cn}/>
  );
};

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
    };
  }

  onMouseEnter = () =>{
    this.setState({ hover: true });
  }
  onMouseLeave = () =>{
    this.setState({ hover: false });
  }
  setTodoItemColor = (rgba) => setTimeout( ()=>{
    const backgroundColor = `rgba( ${rgba.join(',')} )`

    this.bgColor = this.props.completed ? 'rgb(189, 189, 189)' : backgroundColor;
    this._item.style.backgroundColor = this.bgColor;
  })

  render() {
    const { id, text, completed, eTime } = this.props;
    const { hover } = this.state;

    const itemCn = cx( st.item, {
      [st.item_completed]: completed,
      [st.item_hover]: hover
    });

    const textCn = cx( st.itemText, {
      [st.itemText_completed]: completed
    });

    const deleteBtnCn = cx( st.deleteBtn, {
      [st.deleteBtn_hover]: hover
    });

    return (
      <div
        ref={ node=> this._item = node }
        onMouseEnter={ this.onMouseEnter }
        onMouseLeave={ this.onMouseLeave }
        onClick={()=>checkTodo(id)} 
        className={itemCn}
      >

        <CheckBox checked={completed}/>
        <div className={textCn} >{ text }</div>

        <RTime eTime={eTime} setTodoItemColor={this.setTodoItemColor}/>

        <button
          onClick={(e)=>{
            e.stopPropagation();
            deleteTodo(id);
          }}
          className={deleteBtnCn}>X</button>
      </div>
    );
  }
};

@connect( (state,ownProps)=>{

  const _todoList = state.viewMode === 'all' ? state.todoList :
    _.where( state.todoList, { completed: state.viewMode !== 'active' });

  return {
    _todoList: _todoList,
  };
})
class Todo extends React.Component {

  _addTodo = ( e, input ) =>{
    e.preventDefault();
    addTodo( this._input.value );
    this._input.value = '';
    this._input.focus();
  }

  componentDidMount() {
    this._input.focus();
  }

  render() {
    const { _todoList } = this.props;
    const TodoList = _todoList.map( item=> <TodoItem key={item.id} {...item}/> );

    return (
      <div className={st.todo}>
        <form onSubmit={ e=>this._addTodo( e ) }>
          <input
            ref={ node=> this._input = node }
            placeholder='Reading a book until friday 8PM'
            className={st.input}
            type='text'
          />
        </form>
        <div className={st.todoListBox}>
          { TodoList }
        </div>
      </div>
    );
  }
}

export default Todo;



const st = cssInJS({
  todoListBox: {
    margin: '40px 5% 80px 5%',
  },
  'item:hover': {
    transform: 'scale( 1.05 )',
  },
  item: {
    padding: 15,
    display: 'block',
    lineHeight: '1.2',
    borderBottom: '1px solid #ededed',
    textAlign: 'left',
    transition: 'color 0.6s, background-color 0.6s, transform 0.5s',
    backgroundColor: 'rgb(189, 189, 189)',
    height: 60,
    boxSizing: 'border-box',
    cursor: 'pointer',

    //whiteSpace: 'pre-line',
    wordBreak: 'break-all',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',

  },
  item_completed: {

  },
  item_hover: {
    transform: 'scale( 1.05 )',
  },
  checkBox: {
    height: 40,
    width: 40,
    position: 'absolute',
    //left: '5%',
    marginTop: -5,
    content: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="-10 -18 100 135"><circle cx="50" cy="50" r="50" fill="none" stroke="#ededed" stroke-width="3"/></svg>')`,
    WebkitAppearance: 'none',
    paddindRight: 6,
  },
  checkBox_checked: {
    content: `url( 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="-10 -18 100 135"><circle cx="50" cy="50" r="50" fill="none" stroke="#bddad5" stroke-width="3"/><path fill="#5dc2af" d="M72 25L42 71 27 56l-4 4 20 20 34-52z"/></svg>' )`,
  },
  deleteBtn: {
    color: '#cc9a9a',
    display: 'none',
    width: 40,
    height: 40,
    position: 'absolute',
    marginTop: -5,
    marginRight: 5,
    right: 0,
    fontSize: 24,
    backgroundColor: 'transparent',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
    opacity: .7,
  },
  deleteBtn_hover: {
    display: 'inline-block'
  },
  'deleteBtn:hover': {
    opacity: 1,
    transform: 'scale( 1.5 )',
  },
  itemText: {
    color: 'white',
    fontSize: 24,
    position: 'absolute',
    marginLeft: 57,
  },
  itemText_completed: {
    color: '#d9d9d9',
    textDecoration: 'line-through',
  },
  rTime: {
    position: 'relative',
    float: 'right',
    top: 26,
    fontSize: 14,
    marginTop: -3,
    color: 'gray',
  },
  todo: {
    marginTop: 40,
  },
  input: {
    paddingLeft: 10,
    height: 56,
    width: '90%',
    fontSize: 32,
  },
});
