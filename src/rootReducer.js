import { combineReducers } from 'redux';
import _ from 'underscore';

const initUI=()=>({
  width: 0,
  height: 0,
});
const uiReducer = ( state=initUI(), action )=>{

  return state;
};

const todoItemReducer = ( state={}, action )=>{

  if( state.id && action.id !== state.id ) {
    return state;
  }

  switch( action.type ) {
  case 'ADD_TODO':
    state = { ...action.todo, completed: false };
    break;

  case 'CHECK_TODO':
    state = {
      ...state,
      completed: ! state.completed,
      // finish Time
      fTime: state.completed ? null : Date.now(),
    };
    break;

  default:
    break;
  }

  return state;
};

const todoListReducer = ( state=[], action )=>{

  switch( action.type ) {
  case 'ADD_TODO':
    state = [
      ...state,
      todoItemReducer( void 0, action ),
    ];
    break;

  case 'DELETE_TODO':
    const idx = _.findIndex( state, {id: action.id });
    state = [
      ...state.slice(0,idx),
      ...state.slice(idx+1),
    ];
    break;

  case 'CHECK_TODO':
    state = state.map( item=> todoItemReducer( item, action ));
    break;

  case 'CLEAR_COMPLETED':
    state = _.reject( state, { completed: true });
    break;

  default:
    break;
  }
  return state;
};

const viewModeReducer = (state='all', action)=>{

  switch( action.type ) {
  case 'CHANGE_VIEW_MODE':
    state = action.viewMode;
    break;

  default:
    break;
  }
  return state;
}

export default combineReducers({
  ui: uiReducer,
  todoList: todoListReducer,
  viewMode: viewModeReducer,
});
