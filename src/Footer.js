import React from 'react';
import _ from 'underscore';
import cx from 'classnames';
import { connect } from 'react-redux';
import { changeViewMode, clearCompleted } from '_src/action';


const ViewModeBtn = connect( (state,ownProps)=>{

  return {
    active: state.viewMode === ownProps.str,
  };

})( props=>{

  const { str, active } = props;
  const cn = cx( st.viewModeBtn, {
    [st.viewModeBtn_active]: active,
  });

  return (
    <span
      className={ cn }
      onClick={ ()=>changeViewMode(str) }
    >
      { str }
    </span>
  );
});


const Footer = connect( (state,ownProps)=>{

  return {
    leftItemCount: _.where( state.todoList, { completed: false }).length,
  };

})( props=>{

  const { leftItemCount } = props;

  return (
    <div className={st.footer}>


      <div className={st.leftItem}>{ leftItemCount + " item(s) left"}</div>
      <div
        onClick={ () => clearCompleted() }
        className={ st.clearComplatedBtn }
      >
        Clear Completed
      </div>

      <div className={ st.viewModeBtnBox }>
        <ViewModeBtn str={'all'} />
        <ViewModeBtn str={'active'} />
        <ViewModeBtn str={'completed'} />
      </div>

    </div>
  );
});

export default Footer;

const st = cssInJS({

  footer: {
    color: '#222222',
		position: 'fixed',
    textAlign: 'center',
    padding: 15,
    width: '100%',
    height: 20,
    left: 0,
    bottom: 0,
    backgroundColor: '#61dafb',
    minWidth: 640,
  },

  leftItem: {
    position: 'absolute',
    left: '5%',
  },

  viewModeBtnBox: {
    margin: '0 auto',
    position: 'relative',
    width: 400,
  },
  viewModeBtn: {
    margin: '0px 5px',
    left: 0,
    padding: 5,
    cursor: 'pointer',
  },

  viewModeBtn_active: {
    border: '1px solid gray',
    borderRadius: 4,
  },

  clearComplatedBtn: {
    cursor: 'pointer',
    position: 'absolute',
    right: '5%',
    zIndex: 1,
  },
});
