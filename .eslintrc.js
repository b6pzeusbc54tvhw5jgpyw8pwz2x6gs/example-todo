module.exports = {
  extends: "react-app",
  rules: {

    "prefer-const": [2, {"ignoreReadBeforeAssign": false }],
    "no-use-before-define": 0,
    "no-unused-expressions": [2, { "allowShortCircuit": true }],
  },


  globals: {
    cssInJS: true,
    __DEVELOPMENT__: true,
    __PRODUCTION__: true,
  },
};
